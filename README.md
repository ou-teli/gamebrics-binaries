# Gamebrics binaries

This repository contains the media files of an Emergo game (`EduMythbusters`) equipped with Gamebrics components. The game illustrates how a teacher can configure the Gamebrics components, and how, when playing the game in the Emergo player, the player's progress is tracked in the Gamebrics dashboard.

Further instructions on how to install and use the demo game can be found at [the gamebrics docker compose repository](https://gitlab.com/ou-teli/gamebrics-docker-compose).
